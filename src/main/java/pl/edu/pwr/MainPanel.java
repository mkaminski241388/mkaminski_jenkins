package pl.edu.pwr;


import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import library.Library;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

public class MainPanel {
    String path;
    String content;

    public Scene makeScene() {
        LibraryLocal functionLibrary = new LibraryLocal();

        GridPane grid = new GridPane();
        grid.setPadding(new Insets(20, 20, 20, 20));

        Label titleLabel = new Label("Podaj ścieżkę do pliku: ");
        GridPane.setConstraints(titleLabel,0,0);

        TextField field = new TextField();
        field.setPrefWidth(300);
        GridPane.setConstraints(field,1,0);

        CheckBox chMistake = new CheckBox("'ch' <-> 'h'");
        GridPane.setConstraints(chMistake,0,2);

        CheckBox uMistake = new CheckBox("'u' <-> 'ó'");
        GridPane.setConstraints(uMistake,0,3);

        CheckBox czechMistake = new CheckBox("Czeski błąd");
        GridPane.setConstraints(czechMistake,0,4);

        CheckBox spaceEater = new CheckBox("Zjedz spację");
        GridPane.setConstraints(spaceEater,0,5);

        Button pathButton = new Button("Pobierz plik");
        GridPane.setConstraints(pathButton,0,1);

        pathButton.setOnAction(event -> {
            path = field.getText();
            content = functionLibrary.loadFileFromPath(path);
            System.out.println(content);

        });

        Button textModify = new Button("Przekształć tekst ");
        GridPane.setConstraints(textModify,1,1);

        textModify.setOnAction(event -> {
            System.out.println(uMistake.isSelected());

            try {
                functionLibrary.transformContent(path, content, chMistake.isSelected(), uMistake.isSelected(),
                        czechMistake.isSelected(),spaceEater.isSelected());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });



        grid.getChildren().addAll(titleLabel,field,pathButton,textModify,chMistake,uMistake,czechMistake,spaceEater);



        Scene scene = new Scene(grid, 500, 340);
        return scene;
    }
}
